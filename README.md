# Core Bluetooth Debugging

Core Bluetooth. It's a pain. 

Whether you're a new developer battling with the numerous
delegate calls and reference holding requirements, or a seasoned developer continually
being amazed at how buggy things still are, this project is aimed to help you.

## What is it

Core Bluetooth Debugging is intended to be a comprehensive but simple-to-digest set of
Xcode projects, to be used for both learning Core Bluetooth development and also to
test if a bug you've discovered in your own code exists in a simple project.

Everything is in the `AppDelegate` file. If you're a new developer, please understand this isn't good
practice in a production app, but for the purposes of these simple projects it's perfect
because it's easier to see what's going on.

As I find bugs or quirks I'll add what version OS I originally found it on by 
adding it in a comment likw: `[Sierra beta 4]`

## What's in it

Currently, four Xcode projects:

- Central iOS: An iOS project set up as a `CBCentralManager`, with the background mode enabled.
- Peripheral iOS: An iOS project set up as a `CBPeripheralManager`, with the background mode enabled.
- Central macOS: A macOS project set up as a `CBCentralManager`.
- Peripheral macOS: A macOS project set up as a `CBPeripheralManager`.

## Requirements

These projects were first built using Swift 3 with iOS 10 and macOS Sierra as the target
OSes. Feel free to fork and add backwards compatibility as you need it!