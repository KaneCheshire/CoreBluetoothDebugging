//
//  AppDelegate.swift
//  Central-iOS
//
//  Created by Kane Cheshire on 07/08/2016.
//  Copyright © 2016 Kane Cheshire. All rights reserved.
//

import UIKit
import CoreBluetooth

let serviceUUID = CBUUID(string: "5DDEF34A-B600-4C40-AE28-43E71EEFECD1")
let includedServiceUUID = CBUUID(string: "9999")
let characteristicUUIDString = "399A6FFA-44EA-47AD-A7D9-71964028CA36"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CBCentralManagerDelegate, CBPeripheralDelegate {

  var window: UIWindow?
  var centralManager: CBCentralManager!

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
    centralManager = CBCentralManager(delegate: self, queue: nil, options: [CBCentralManagerOptionRestoreIdentifierKey : "RestorationIdentifier"])
    return true
  }

  func applicationWillTerminate(_ application: UIApplication) {
    cleanup()
  }
  
  
  
  
  
  
  
  
  
  
  func scan() {
    print("Scanning...")
    centralManager.scanForPeripherals(withServices: [serviceUUID], options: nil)
  }
  
  func stopScan() {
    print("Stopping scan...")
    centralManager.stopScan()
  }
  
  func cleanup() {
    print("Cleaning up...")
    centralManager.stopScan()
    discoveredPeripherals.removeAll()
  }
  
  
  
  
  
  
  
  
  
  // MARK: CBCentralManagerDelegate -
  
  func centralManagerDidUpdateState(_ central: CBCentralManager) {
    print("Updated state: \(central.state.rawValue)")
    switch central.state {
    case .poweredOn:
      print("Powered on")
      scan()
    default:
      stopScan()
    }
  }

  func centralManager(_ central: CBCentralManager, willRestoreState dict: [String : Any]) {
    print("Will restore state \(dict)")
  }
  
  var discoveredPeripherals = Set<CBPeripheral>()
  
  func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
    print("Discovered: \(peripheral)")
    discoveredPeripherals.insert(peripheral)
    print("Attempting to connect to \(peripheral.identifier.uuidString)")
    centralManager.connect(peripheral, options: nil)
  }
  
  func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
    print("Connected to \(peripheral.identifier.uuidString)")
    peripheral.delegate = self
    peripheral.discoverServices([serviceUUID])
  }
  
  func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
    print("Failed to connect to \(peripheral.identifier.uuidString) \(error)")
    discoveredPeripherals.remove(peripheral)
  }
  
  func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
    print("Disconnected from \(peripheral.identifier.uuidString) \(error)")
    discoveredPeripherals.remove(peripheral)
  }
  
  
  
  
  
  
  
  
  // MARK: CBPeripheralDelegate -
  
  // MARK: Services
  
  func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
    print("Discovered services \(peripheral.services) for \(peripheral.identifier.uuidString) \(error)")
    guard let services = peripheral.services else { preconditionFailure("No services") }
    for service in services {
      guard service.uuid.uuidString == serviceUUID.uuidString else { continue }
      print("Attempting to discover included services for peripheral: \(peripheral.identifier.uuidString)")
      peripheral.discoverIncludedServices(nil, for: service)
    }
  }
  
  func peripheral(_ peripheral: CBPeripheral, didDiscoverIncludedServicesFor service: CBService, error: Error?) {
    print("Discovered included services for peripheral: \(peripheral.identifier.uuidString) \(error)")
    guard let services = service.includedServices else { preconditionFailure("No services") }
    for includedService in services {
      print("Included service: \(includedService)")
      guard includedService.uuid.uuidString == includedServiceUUID.uuidString else { continue }
      print("Attempting to discover characteristics for peripheral: \(peripheral.identifier.uuidString)")
      peripheral.discoverCharacteristics(nil, for: includedService)
    }
  }
  
  func peripheral(_ peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService]) {
    print("\(peripheral.identifier.uuidString) modified services: \(invalidatedServices)")
    let invalidatedServicesIncludesOurService = invalidatedServices.contains { $0.uuid.uuidString == serviceUUID.uuidString }
    guard invalidatedServicesIncludesOurService else { return }
    
    // Rediscovering services immediately is what the docs say to do, however this doesn't work if the macOS app is
    // relaunched, so instead we'll disconnect and wait for a reconnection...
    centralManager.cancelPeripheralConnection(peripheral)
  }
  
  // MARK: Characteristics
  
  func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
    print("Discovered characteristics for peripheral: \(peripheral.identifier.uuidString)")
    guard let characteristics = service.characteristics else { preconditionFailure("No characteristics") }
    for characteristic in characteristics {
      print("Discovered characteristic: \(characteristic.uuid.uuidString)")
      print("Attempting to enable notifications (subscribe) to characteristic...")
      peripheral.setNotifyValue(true, for: characteristic)
    }
  }
  
  func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
    print("Peripheral: \(peripheral.identifier.uuidString) updated notification state for characteristic: \(characteristic.uuid.uuidString)")
    switch characteristic.isNotifying {
    case false:
      print("No longer subscribed")
    case true:
      print("Now subscribed, attempting to write value...")
      peripheral.writeValue("Hello World".data(using: String.Encoding.utf8)!, for: characteristic, type: .withResponse)
    }
  }
  
  // MARK: Writing and reading characteristic values
  
  func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
    guard error == nil else {
      print("Peripheral: \(peripheral.identifier.uuidString) errored while writing value for characteristic \(characteristic.uuid.uuidString). \(error!)")
      return
    }
    print("Peripheral: \(peripheral.identifier.uuidString) wrote value for characteristic \(characteristic.uuid.uuidString)")
  }
  
  func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
    guard error == nil else {
      print("Peripheral: \(peripheral.identifier.uuidString) errored updating value for characteristic \(characteristic.uuid.uuidString). \(error!)")
      return
    }
    print("Peripheral: \(peripheral.identifier.uuidString) updated value for characteristic \(characteristic.uuid.uuidString)")
  }
  
  // MARK: Descriptors
  
  func peripheral(_ peripheral: CBPeripheral, didDiscoverDescriptorsFor characteristic: CBCharacteristic, error: Error?) {
    print("Discovered descriptors")
  }
  
  // MARK: Writing and reading descriptor values
  
  
  func peripheral(_ peripheral: CBPeripheral, didWriteValueFor descriptor: CBDescriptor, error: Error?) {
    print("Wrote value for descriptor")
  }
  
  func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor descriptor: CBDescriptor, error: Error?) {
    print("Updated value for descriptor")
  }
  
  // MARK: Everything else (so many delegate methods!)
  
  func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {
    print("Read RSSI")
  }
  
  func peripheralDidUpdateName(_ peripheral: CBPeripheral) {
    print("Updated name")
  }


}

