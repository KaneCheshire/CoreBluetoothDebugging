//
//  AppDelegate.swift
//  Peripheral-iOS
//
//  Created by Kane Cheshire on 17/03/2017.
//  Copyright © 2017 Kane Cheshire. All rights reserved.
//

import UIKit
import CoreBluetooth

let primaryServiceUUID = CBUUID(string: "5DDEF34A-B600-4C40-AE28-43E71EEFECD1")
let includedServiceUUID = CBUUID(string: "9999") // If we use a full-length UUID here, Core Bluetooth seems to change the first half when publishing it! [Sierra beta 4]
let primaryCharacteristicUUID = CBUUID(string: "399A6FFA-44EA-47AD-A7D9-71964028CA36")

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CBPeripheralManagerDelegate {

  var window: UIWindow?
  var peripheralManager: CBPeripheralManager!

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    peripheralManager = CBPeripheralManager(delegate: self, queue: nil)
    return true
  }
  
  func applicationWillTerminate(_ application: UIApplication) {
    stopAdvertising()
  }


  let primaryService = CBMutableService(type: primaryServiceUUID, primary: true)
  
  let includedService = CBMutableService(type: includedServiceUUID, primary: true) // Needs to also be set as primary otherwise characteristics don't get published [Sierra beta 4]
  
  let primaryCharacteristic = CBMutableCharacteristic(type: primaryCharacteristicUUID,
                                                      properties: [.read, .write, .notify],
                                                      value: nil,
                                                      permissions: [.readable, .writeable])
  
  func advertise() {
    stopAdvertising()
    print("Attempting to advertise...")
    includedService.characteristics = [primaryCharacteristic]
    peripheralManager.add(includedService)
  }
  
  func stopAdvertising() {
    print("Stopping advertising...")
    peripheralManager.removeAllServices()
    peripheralManager.stopAdvertising()
  }
  
  
  
  
  
  
  
  
  
  
  
  
  // MARK: CBPeripheralManagerDelegate -
  
  func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
    print("Updated state \(peripheral.state.rawValue)")
    switch peripheral.state {
    case .poweredOn:
      print("Powered on")
      advertise()
    default:
      print("Not powered on")
      stopAdvertising()
    }
  }
  
  func peripheralManager(_ peripheral: CBPeripheralManager, didAdd service: CBService, error: Error?) {
    guard error == nil else {
      print("Error adding service: \(service) \(error!)")
      return
    }
    print("Added service \(service)")
    guard service == primaryService else {
      primaryService.includedServices = [service]
      peripheral.add(primaryService)
      return
    }
    let adData = [CBAdvertisementDataServiceUUIDsKey : [primaryServiceUUID]]
    peripheral.startAdvertising(adData)
  }
  
  func peripheralManagerDidStartAdvertising(_ peripheral: CBPeripheralManager, error: Error?) {
    guard error == nil else {
      print("Error advertising: \(error!)")
      return
    }
    print("Started advertising")
  }
  
  func peripheralManagerIsReady(toUpdateSubscribers peripheral: CBPeripheralManager) {
    print("Ready to update subscribers")
  }
  
  func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveRead request: CBATTRequest) {
    print("Received read request \(request)")
  }
  
  func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveWrite requests: [CBATTRequest]) {
    print("Received write requests...")
    for request in requests {
      print("Request: \(request). Data: \(request.value)")
      peripheral.respond(to: request, withResult: .success)
    }
  }
  
  func peripheralManager(_ peripheral: CBPeripheralManager, willRestoreState dict: [String : Any]) {
    print("Will restore state: \(dict)")
  }
  
  func peripheralManager(_ peripheral: CBPeripheralManager, central: CBCentral, didSubscribeTo characteristic: CBCharacteristic) {
    print("Central: \(central.identifier.uuidString) SUBSCRIBED to \(characteristic.uuid.uuidString)")
  }
  
  func peripheralManager(_ peripheral: CBPeripheralManager, central: CBCentral, didUnsubscribeFrom characteristic: CBCharacteristic) {
    print("Central: \(central.identifier.uuidString) UNSUBSCRIBED to \(characteristic.uuid.uuidString)")
  }


}

